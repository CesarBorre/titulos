package com.neology.titulos;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

import com.neology.titulos.model.Titulos_WS_Data;

public class DataActivity extends AppCompatActivity {
    Titulos_WS_Data titulosData;
    TextView nombre;
    TextView nombreUni;
    TextView titulo;
    TextView secretario;
    TextView rector;
    ImageView fotoTitulado;
    ImageView firmaDigital;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        Intent intent = getIntent();
        titulosData = (Titulos_WS_Data) intent.getSerializableExtra("titulosData");
        init();
    }

    private void init() {
        nombre = (TextView) findViewById(R.id.nombreID);
        nombre.setText(titulosData.getNombre() + " " + titulosData.getApePaterno() + " " + titulosData.getApeMaterno());
        titulo = (TextView) findViewById(R.id.tituloID);
        titulo.setText(titulosData.getTitulo());
        nombreUni = (TextView) findViewById(R.id.nombreUniID);
        nombreUni.setText(titulosData.getNombreUniversidad());
        setFont(nombreUni, 0);
        fotoTitulado = (ImageView) findViewById(R.id.fotoTituladoID);
        setImg(titulosData.getFotoMiniatura(), fotoTitulado);
        firmaDigital = (ImageView) findViewById(R.id.firmaDigitalID);
        setImg(titulosData.getFirmaDigital(), firmaDigital);
        rector = (TextView) findViewById(R.id.rectorID);
        setFont(rector, 1);
        secretario = (TextView)findViewById(R.id.secretarioGralID);
        setFont(secretario, 1);
    }

    private void setFont(TextView t, int fontType) {
        Typeface type = null;
        switch (fontType) {
            case 0:
                type = Typeface.createFromAsset(getAssets(), "fonts/old_london.ttf");
                break;
            case 1:
                type = Typeface.createFromAsset(getAssets(), "fonts/mi.otf");
                break;
        }
//        TextView myTextview;
//        myTextview= (TextView) findViewById(R.id.pruebaFont1);
//        String htmltext = "<b>El</b> Rector de la Universidad por cuanto:";
//        Spanned sp = Html.fromHtml(htmltext);
//        myTextview.setText(sp);
        t.setTypeface(type);
    }

    private void setImg(byte[] b, ImageView imageView) {
        byte[] decodedString = Base64.decode(b, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imageView.setImageBitmap(bitmap);
    }
}
