package com.neology.titulos.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 18/05/16.
 */
public class Titulos_Chip_Data implements Parcelable{
    private String titulo;
    private String nombre;
    private String carrera;
    private String nivel;
    private int numIdentificación;
    private int claveEscuela;

    public Titulos_Chip_Data (
            String titulo,
            String nombre,
            String carrera,
            String nivel,
            int numIdentificación,
            int claveEscuela) {
        this.titulo = titulo;
        this.nombre = nombre;
        this.carrera = carrera;
        this.nivel = nivel;
        this.numIdentificación = numIdentificación;
        this.claveEscuela = claveEscuela;
    }

    protected Titulos_Chip_Data(Parcel in) {
        titulo = in.readString();
        nombre = in.readString();
        carrera = in.readString();
        nivel = in.readString();
        numIdentificación = in.readInt();
        claveEscuela = in.readInt();
    }

    public static final Creator<Titulos_Chip_Data> CREATOR = new Creator<Titulos_Chip_Data>() {
        @Override
        public Titulos_Chip_Data createFromParcel(Parcel in) {
            return new Titulos_Chip_Data(in);
        }

        @Override
        public Titulos_Chip_Data[] newArray(int size) {
            return new Titulos_Chip_Data[size];
        }
    };

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public int getNumIdentificación() {
        return numIdentificación;
    }

    public void setNumIdentificación(int numIdentificación) {
        this.numIdentificación = numIdentificación;
    }

    public int getClaveEscuela() {
        return claveEscuela;
    }

    public void setClaveEscuela(int claveEscuela) {
        this.claveEscuela = claveEscuela;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(titulo);
        dest.writeString(nombre);
        dest.writeString(carrera);
        dest.writeString(nivel);
        dest.writeInt(numIdentificación);
        dest.writeInt(claveEscuela);
    }
}
