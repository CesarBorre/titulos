package com.neology.titulos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neology.titulos.dialogs.ConfigDIalog;
import com.neology.titulos.model.Titulos_Chip_Data;
import com.neology.titulos.model.Titulos_WS_Data;
import com.neology.titulos.utils.CheckInternetConnection;
import com.neology.titulos.utils.SnackBar;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    static final String TAG = MainActivity.class.getSimpleName();

    ProgressDialog progressDialog;
    Titulos_Chip_Data titulos_chip_data;

    TextView titulo;
    TextView nombre;
    TextView carrera;
    TextView nivel;
    TextView numIdentificacion;
    TextView clave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolbar();
        initElements();

        Intent intent = getIntent();
        titulos_chip_data = intent.getParcelableExtra("dataChip");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
//        MenuItem item = menu.findItem(R.id.action_login);
//        item.setActionView(R.layout.item_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.buscar_item:
                searchData();
                return true;
//            case R.id.action_settings:
//                showDialog_Settings();
////                SnackBar.showSnackBar("Añadir a contactos", this, 1);
//                return true;
//            case R.id.action_favorite:
//                showSnackBar("Añadir a favoritos");
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initElements() {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setTitle(getResources().getString(R.string.title_progress_consulta));
        progressDialog.setMessage(getResources().getString(R.string.msg_progress));

        titulo = (TextView)findViewById(R.id.tituloID);
        nombre = (TextView)findViewById(R.id.nombreID);
        carrera = (TextView)findViewById(R.id.carreraID);
        nivel = (TextView)findViewById(R.id.nivelID);
        numIdentificacion = (TextView)findViewById(R.id.numIdentificacionID);
        clave = (TextView)findViewById(R.id.claveEscuela);
    }

    private void searchData() {
        if (CheckInternetConnection.isConnectedToInternet(getApplicationContext())) {
            getJson();
        } else {
            SnackBar.showSnackBar(getResources().getString(R.string.no_internet), this, 1);
        }
    }

    private void getJson() {
        progressDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.GET,
                "http://52.39.221.86:8080/api/titulo?intNumeroIdentificacion=2",
//                "http://" + preferences.Get_stringfrom_shprf(Constants_Settings.KEY_URL) + "/api/carneAll?intNumeroIdentificacion=4842015000102016",
                null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        readJson(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Error al conectar con servidor", Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        VolleyApp.getmInstance().addToRequestQueue(jsonObjReq);
    }

    private void readJson(String json) {
        WS ws = new WS();
        ws.execute(json);
    }

    class WS extends AsyncTask<String, Void, Boolean> {
        Titulos_WS_Data titulosData;

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                JSONObject object = new JSONObject(params[0]);
                JSONObject jsonObject = object.getJSONObject("titulos");
                titulosData = new Titulos_WS_Data(
                        jsonObject.getInt("intNumeroIdentificacion"),
                        jsonObject.getString("strNombre"),
                        jsonObject.getString("strApePaterno"),
                        jsonObject.getString("strApeMaterno"),
                        jsonObject.getString("strTitulo"),
                        jsonObject.getString("strCarrera"),
                        jsonObject.getString("strNivel"),
                        jsonObject.getInt("intNumFolioDocumento"),
                        jsonObject.getString("strNacionalidad"),
                        jsonObject.getString("genero"),
                        jsonObject.getLong("dFechaNac"),
                        jsonObject.getLong("dFechaExpedicion"),
                        jsonObject.getString("strNombreUniversidad"),
                        jsonObject.getInt("intClaveUniversidad"),
                        jsonObject.getString("bFotoOriginal").getBytes(),
                        jsonObject.getString("bFotoMiniatura").getBytes(),
                        jsonObject.getString("bFirmaTitulado").getBytes()
                );

            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean b) {
            super.onPostExecute(b);
            progressDialog.dismiss();
            if (b) {
                Intent intent = new Intent(getApplicationContext(), DataActivity.class);
                intent.putExtra("titulosData", titulosData);
                startActivity(intent);
            }
        }
    }

    private void setToolbar() {
        // Añadir la Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

//        if (getSupportActionBar() != null) // Habilitar up button
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        CollapsingToolbarLayout collapser =
//                (CollapsingToolbarLayout) findViewById(R.id.collapser);
//        collapser.setTitle("UMSA"); // Cambiar título
    }

    private void showDialog_Settings() {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = ConfigDIalog.newInstance();
        newFragment.show(ft, "dialog");
    }

    public void ok() {
//        Intent i = getPackageManager().getLaunchIntentForPackage( getPackageName() );
        Intent i = new Intent(getApplicationContext(), SplashActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
