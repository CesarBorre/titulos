package com.neology.titulos;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.NfcF;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.neology.titulos.lecturanfc.LecturaTag;
import com.neology.titulos.model.Titulos_Chip_Data;
import com.neology.titulos.utils.SnackBar;

public class ReadTagActivity extends AppCompatActivity {

    public static final String TAG = SplashActivity.class.getSimpleName();
    private CountDownTimer mCountDownTimer;

    NfcAdapter nfcAdapter = null;
    PendingIntent pendingIntent = null;
    IntentFilter[] filters = null;
    String[][] techList = null;
    protected String[] datosTag;
    Intent lecturaIntent;

    Toolbar mToolbar;
    ProgressDialog progressDialog;

    ImageView fondo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_tag);
        initNFC();

//        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//
//        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        init();

    }

    private void initNFC() {
        // Obtenemos el control sobre el lector de NFC
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        // Si no se encuentra el lector de NFC se cierra aplicacion
        if (nfcAdapter == null) {
            Toast.makeText(this, getString(R.string.nfc_warning),
                    Toast.LENGTH_LONG).show();
            finish();
            return;
        } else if(!nfcAdapter.isEnabled()) {
            SnackBar.showSnackBar(getResources().getString(R.string.activate_nfc), this, 0);
        }

        // Creamos un Intent para manejar los datos leidos
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // Creamos un filtro de Intent relacionado con descubrir un mensaje NDEF
        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter discovery = new IntentFilter(
                NfcAdapter.ACTION_TAG_DISCOVERED);

        // Configuramos el filtro para que acepte de cualquier tipo de NDEF
        try {
            ndef.addDataType("*/*");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("fail", e);
        }
        filters = new IntentFilter[]{ndef, discovery};

        // Configuramos para que lea de cualquier clase de tag NFC
        techList = new String[][]{new String[]{NfcF.class.getName()}};

        Intent lecturaIntent = getIntent();
        Log.d(TAG, "INTENT " + lecturaIntent.toString());
        Log.d(TAG, "PENDING INTENT " + pendingIntent.toString());
        Parcelable[] rawMsgs = lecturaIntent
                .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        Log.d(TAG, "RAW " + rawMsgs);

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(lecturaIntent.getAction())) {
            lecturaNFC(lecturaIntent);
        }
    }

    private void init() {
        progressDialog = new ProgressDialog(ReadTagActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setTitle(getResources().getString(R.string.title_progress));
        progressDialog.setMessage(getResources().getString(R.string.msg_progress));

//        fondo = (ImageView) findViewById(R.id.fondoID);
//        fondo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                progressDialog.show();
//                mCountDownTimer = new CountDownTimer(600, 500) {
//                    @Override
//                    public void onTick(long millisUntilFinished) {
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        Intent intent = new Intent(ReadTagActivity.this, MainActivity.class);
//                        startActivity(intent);
//                        overridePendingTransition(R.animator.animation, 0);
//                        progressDialog.dismiss();
//                    }
//                }.start();
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_read_tag, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void lecturaNFC(Intent intent) {
        try {

            LecturaTag lecturaObj = new LecturaTag(intent);
            datosTag = lecturaObj.lectura();

            if (datosTag != null) {

                Log.d(TAG, "# Datos leidos: " + datosTag.length);
                int i = 0;
                while (i < datosTag.length) {
                    Log.d(TAG, "Dato[" + i + "]: " + datosTag[i]);
                    i++;
                }
                Titulos_Chip_Data titulos_chip_data = new Titulos_Chip_Data(
                        datosTag[0],
                        datosTag[1],
                        datosTag[2],
                        datosTag[3],
                        Integer.parseInt(datosTag[4]),
                        Integer.parseInt(datosTag[5])
                );
                Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
                intent1.putExtra("dataChip", titulos_chip_data);
                startActivity(intent1);
            }
        } catch (Exception e) {
            Log.e(TAG, "Excepion::lecturaNFC->" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart()");
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Log.d(TAG, "onResume()");
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, filters,
                techList);
        lecturaNFC(lecturaIntent);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Log.d(TAG, "onPause()");
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Log.d(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, "onNewIntent()");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        lecturaNFC(intent);
    }
}
